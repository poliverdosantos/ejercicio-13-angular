import { Component, OnInit } from '@angular/core';
import { Iformulario } from '../interfases/formulario.interfaces';
@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

    validar : Iformulario = {
    formularioCorreo : '' 
  };

  verificar : boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  guardar():void{
    this.verificar = true
    console.log("correo verificado");
    
  }

}
